// Micro Modal [https://micromodal.vercel.app/]
MicroModal.init({
  onShow: modal => $('.screens-slider').slick('setPosition'),
  disableScroll: true, // default - false
  awaitOpenAnimation: true, // default - false
  awaitCloseAnimation: true // default - false
});

// Slick Slider [https://kenwheeler.github.io/slick/]
$(document).ready(function(){
  let prevButton = '<span class="slick-prev icon-chevron-thin-left"><i class="icono-arrow-right"></i></span>';
	let nextButton = '<span class="slick-next icon-chevron-thin-right"><i class="icono-arrow-left"></i></span>';
  $('.screens-slider').slick({
    prevArrow: prevButton,
    nextArrow: nextButton,
    centerMode: true,
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          centerMode: false
        }
      }
    ]
  });
});

// Prevent default for modal trigger button
const btnModal = document.querySelector('.btn-modal');
btnModal.addEventListener('click', (event) => {
  event.preventDefault();
});
